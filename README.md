# Dataloop.ai Assignment

This repository contains the implementation of a Pythonic data model class called Data, which satisfies the given requirements from Dataloop.ai. The Data class is designed to handle loading and saving data from/to dictionaries, instantiation, default values for attributes, autocompletion, and dynamic usability for defining new data structures.
Requirements

The Data class fulfills the following requirements:

1. Loading and Saving: The class provides methods to load data from a dictionary (from_dict) and save data to a dictionary (to_dict).

2. Direct Instantiation: The class supports direct instantiation, allowing you to create an instance of Data without the need for additional steps or dependencies.

3. Default Values: You can define default values for attributes within the Data class. These default values will be used if an attribute is not explicitly set.

4. Autocomplete: The Data class incorporates autocomplete functionality, enhancing the development experience by providing suggestions for attribute names and values.

5. Dynamic and General Usability: The class is designed to be flexible and adaptable, allowing easy definition of new data structures. You can utilize the Data class to handle various types of data and structures with minimal code modifications.

6. Reflect Inner Value: The Data class provides the ability to reflect the inner value on the main level. This means that the attributes of the Data object can directly represent the values stored within the data structure.


# Implementations

There are two implementations of the Data class available in this repository:

`data_class_one.py`: This implementation is straightforward and follows a simple approach to fulfill the requirements of the Data class. It can be found in the file data_class_one.py.

`data_class_two.py`: This alternative implementation explores an alternative approach to achieving the functionalities of the Data class. It can be found in the file data_class_two.py. Please note that this implementation may have different trade-offs or design choices compared to data_class_one.py.


# Usage

To use the Data class, follow these steps:

Clone this repository:
```bash
git clone git@gitlab.com:omidroshani/dataloop.ai-assignment.git
```
Import the Data class into your Python script, you can see the sample in the `main.py`

```python
from data_class_one import Data
```
Instantiate a Data object:
```python
data = {
    "id": "1",
    "name": "first",
    "metadata": {
        "system": {
            "size": 10.7
        },
        "user": {
            "batch": 10
        }
    }
}

# Load from dict
my_inst_1 = Data.from_dict(data)
```
Or
```python
my_inst_2 = Data(name="my")
```
Access and manipulate the data using the data object's attributes.
```python
my_inst_1.metadata.system.height = 200
print(my_inst_1.metadata.system.height)

```



# Example

Here's a simple example to demonstrate the usage of the Data class:
```python
from data_class_one import Data
# from data_class_two import Data


data = {
    "id": "1",
    "name": "first",
    "metadata": {
        "system": {
            "size": 10.7
        },
        "user": {
            "batch": 10
        }
    }
}

# Load from dict
my_inst_1 = Data.from_dict(data)
print('1st Instance Details: ', my_inst_1.to_dict())

# Load from inputs
my_inst_2 = Data(name="my")
print('2nd Instance Details: ', my_inst_2.to_dict())

# Reflect inner value
print('Inner level value: ', my_inst_1.metadata.system.size, ', Main Level Value:', my_inst_1.size )

# Default values
print('Default Value for height: ', my_inst_1.metadata.system.height)

my_inst_1.metadata.system.height = 200
print('Modify height value: ', my_inst_1.metadata.system.height)


print('Get height value by converting to dict: ', my_inst_1.to_dict()['metadata']['system']['height'])

# Autocomplete
print('Check autocomplete: ', my_inst_1.metadata.system)
```
Output:
```
1st Instance Details:  {'id': '1', 'name': 'first', 'metadata': {'system': {'size': 10.7, 'height': 100.0}, 'user': {'batch': 10}}}
2nd Instance Details:  {'id': '', 'name': 'my', 'metadata': {'system': {'size': 0.0, 'height': 100.0}, 'user': {'batch': 0}}}
Inner level value:  10.7 , Main Level Value: 10.7
Default Value for height:  100.0
Modify height value:  200
Get height value by converting to dict:  200
Check autocomplete:  System(size=10.7, height=200)
```
