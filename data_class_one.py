from dataclasses import dataclass, fields, is_dataclass, field
from typing import Any, Dict


@dataclass
class BaseDataClass:
    
    '''
    Implemented this base class to use it as a base class for next implementations, it handles nested objects or dictionaries, accessing internal values, etc.
    '''

    @classmethod
    def from_dict(cls, data: Dict[str, Any]):
        kwargs = {}
        for field in fields(cls):
            if isinstance(data.get(field.name), dict) and is_dataclass(field.type):
                kwargs[field.name] = field.type.from_dict(data.get(field.name, {}))
            else:
                kwargs[field.name] = data.get(field.name, field.default)
        return cls(**kwargs)

    def to_dict(self):
        result = {}
        for field in fields(self):
            value = getattr(self, field.name)
            if is_dataclass(value):
                result[field.name] = value.to_dict()
            else:
                result[field.name] = value
        return result

    def __getattr__(self, item):
        for field in fields(self):
            value = getattr(self, field.name)
            if is_dataclass(value):
                try:
                    return getattr(value, item)
                except AttributeError:
                    pass
        raise AttributeError(f"'{type(self).__name__}' object has no attribute '{item}'")


@dataclass
class System(BaseDataClass):
    size: float = 0.0
    height: float = 100.0


@dataclass
class User(BaseDataClass):
    batch: int = 0


@dataclass
class Metadata(BaseDataClass):
    system: System = field(default_factory=System)
    user: User = field(default_factory=User)


@dataclass
class Data(BaseDataClass):
    id: str = ""
    name: str = ""
    metadata: Metadata = field(default_factory=Metadata)
