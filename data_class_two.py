class Data:
    def __init__(self, **kwargs):
        self._data = kwargs # initiate the instance with kwargs that is a dictionary
        self._set_default_values() # set default values if user does not specified

    @classmethod
    def from_dict(cls, data):
        return cls(**data) # initiate data object from a dictionary

    def to_dict(self):
        return self._convert_to_dict(self._data) # convert data instance to a dictionary

    def _set_default_values(self):
        if 'metadata' not in self._data: # Check if user specified metadata
            self._data['metadata'] = {}
        metadata = self._data['metadata']
        if 'system' not in metadata: # Check if user specified system
            metadata['system'] = {}
        system = metadata['system']
        system.setdefault('height', 100) # Assign default value for height if user does not specified

    def _convert_to_dict(self, data):
        if isinstance(data, Data): # use recursive approach to convert nested objects to dictionary
            return self._convert_to_dict(data._data)
        elif isinstance(data, dict): # if we end with a dictionary then we return it and go back
            return {k: self._convert_to_dict(v) for k, v in data.items()}
        else:
            return data

    def __getattr__(self, attr):
        if attr in self._data:
            value = self._data[attr]
            if isinstance(value, dict):
                self._data[attr] = Data(**value)  # store Data object for future accesses
            return self._data[attr]
        elif attr.startswith('_'):
            return super().__getattribute__(attr)
        else:
            for value in self._data.values(): # search for the attribute in nested dictionaries
                if isinstance(value, Data) and hasattr(value, attr):
                    return getattr(value, attr)
            return None

    def __setattr__(self, attr, value):
        if attr == '_data':
            super().__setattr__(attr, value)
        else:
            self._data[attr] = value

    def __dir__(self):
        return self._data.keys() + list(super().__dir__()) # based on the docs that i read, we can implement autocomplete with this method but it depends on the IDE that we use

    def __repr__(self):
        return repr(self._data)
