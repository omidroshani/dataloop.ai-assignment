from data_class_one import Data
# from data_class_two import Data


data = {
    "id": "1",
    "name": "first",
    "metadata": {
        "system": {
            "size": 10.7
        },
        "user": {
            "batch": 10
        }
    }
}

# Load from dict
my_inst_1 = Data.from_dict(data)
print('1st Instance Details: ', my_inst_1.to_dict())

# Load from inputs
my_inst_2 = Data(name="my")
print('2nd Instance Details: ', my_inst_2.to_dict())

# Reflect inner value
print('Inner level value: ', my_inst_1.metadata.system.size, ', Main Level Value:', my_inst_1.size )

# Default values
print('Default Value for height: ', my_inst_1.metadata.system.height)

my_inst_1.metadata.system.height = 200
print('Modify height value: ', my_inst_1.metadata.system.height)


print('Get height value by converting to dict: ', my_inst_1.to_dict()['metadata']['system']['height'])

# Autocomplete
print('Check autocomplete: ', my_inst_1.metadata.system)