# Under development

class MyClass:
    __slots__ = ('required_attr1', 'required_attr2', 'optional_attr1', 'optional_attr2')

    default_values = {
        'optional_attr1': 'default_value',
        'optional_attr2': 'default_value'
    }

    def __init__(self, **kwargs):
        if kwargs:
            self.from_dict(kwargs)

    def from_dict(self, data):
        required_attrs = set(self.__slots__) - set(self.default_values.keys())
        missing_attrs = required_attrs - set(data.keys())
        if missing_attrs:
            raise ValueError(f"Missing value(s) for required attribute(s): {missing_attrs}")
        
        for key in list(self.default_values.keys()):
            setattr(self, key, self.default_values[key])
            
        for key, value in data.items():
            if key not in self.__slots__:
                raise AttributeError(f"Invalid attribute: {key}")
                
            if isinstance(value, dict):
                nested_obj = MyClass()
                nested_obj.from_dict(value)
                setattr(self, key, nested_obj)
                
            else:
                setattr(self, key, value)

    def to_dict(self):
        result = {}
        for attr in self.__slots__:
            value = getattr(self, attr)
            if isinstance(value, MyClass):
                result[attr] = value.to_dict()
            else:
                result[attr] = value
        return result
        
    def __setattr__(self, name, value):
        if isinstance(value, dict):
            nested_obj = MyClass()
            nested_obj.from_dict(value)
            setattr(self, name, nested_obj)
        else:
            super().__setattr__(name, value)

    def __getattr__(self, name):
        return self._bfs_find_key(self.to_dict(), name)


    def _bfs_find_key(self, nested_dict, key):
        queue = [(nested_dict, [])]

        while queue:
            current_dict, path = queue.pop(0)

            for k, v in current_dict.items():
                if k == key:
                    return current_dict[key]

                if isinstance(v, dict):
                    queue.append((v, path + [k]))

        return None

    def _dfs_find_key(self, nested_dict, key):
        stack = [(nested_dict, [])]

        while stack:
            current_dict, path = stack.pop()

            for k, v in current_dict.items():
                if k == key:
                    return current_dict[key]

                if isinstance(v, dict):
                    stack.append((v, path + [k]))

        return None


# class MyClass1(MyClass):
#     fields = {
#         'a': {
#             'type': str
#         },
#         'b': {
#             'type': int,
#             'default': 10
#         }
#     }
        
obj_data = {
    'required_attr1': 'value1',
    'required_attr2': 'value2',
    'optional_attr1': {
        'required_attr1': 'nested_value1',
        'required_attr2': 'nested_value2'
    }
}

obj = MyClass()
obj.from_dict(obj_data)

obj.optional_attr1.required_attr1 = {'required_attr1': 'abc', 'required_attr2':'123'}
result_dict = obj.to_dict()
print(result_dict)